package com.utest.automation.tasks;

import com.utest.automation.userinterface.NextLocation;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;

public class Location implements Task {

    private String city;
    private String zip;
    private String country;

    public Location(String city, String zip, String country) {
        this.city = city;
        this.zip = zip;
        this.country = country;
    }

    public static Location onThePage(String city, String zip, String country){

        return Tasks.instrumented(Location.class,city,zip,country);
    }

    @Override
    public <T extends Actor> void performAs(T actor){
        actor.attemptsTo(Enter.theValue(city).into(NextLocation.INPUT_CITY),
                Hit.the(Keys.ARROW_DOWN).keyIn(NextLocation.SEARCH_CITY),
                Enter.theValue(zip).into(NextLocation.INPUT_ZIP),
                Enter.theValue(zip).into(NextLocation.INPUT_ZIP),
                Click.on(NextLocation.INPUT_COUNTRY),
                Enter.theValue(country).into(NextLocation.INPUT_SEARCH_COUNTRY),
                Click.on(NextLocation.BUTTON_NEXT_DEVICES)
                );
    }
}
