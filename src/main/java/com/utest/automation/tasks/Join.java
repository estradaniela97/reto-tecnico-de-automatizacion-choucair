package com.utest.automation.tasks;

import com.utest.automation.userinterface.UtestJoinPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Join implements Task{

    private String firstName;
    private String lastName;
    private String email;
    private String month;
    private String day;
    private String year;

    public Join(String firstName,String lastName,String email,String month,String day,String year){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.month = month;
        this.day = day;
        this.year = year;
    }
    public static Join onThePage(String firstName,String lastName,String email,String month,String day,String year) {
        return Tasks.instrumented(Join.class,firstName,lastName,email,month,day,year);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(UtestJoinPage.JOIN_BUTTON),
                Enter.theValue(firstName).into(UtestJoinPage.INPUT_FIRST_NAME),
                Enter.theValue(lastName).into(UtestJoinPage.INPUT_LAST_NAME),
                Enter.theValue(email).into(UtestJoinPage.INPUT_EMAIL),
                SelectFromOptions.byValue(month).from(UtestJoinPage.SELECT_BIRTH_MONT),
                SelectFromOptions.byValue(day).from(UtestJoinPage.SELECT_BIRTH_DAY),
                SelectFromOptions.byValue(year).from(UtestJoinPage.SELECT_BIRTH_YEAR),
                Click.on(UtestJoinPage.BUTTON_NEXT)
                );

    }
}
