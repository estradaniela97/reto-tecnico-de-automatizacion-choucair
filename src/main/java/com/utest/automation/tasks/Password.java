package com.utest.automation.tasks;

import com.utest.automation.userinterface.EndPassword;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Password implements Task {

    private String password;
    private String confirmPassword;

    public Password(String password, String confirmPassword) {
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public static Password onThePage(String password, String confirmPassword) {
        return Tasks.instrumented(Password.class,password,confirmPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor){
        actor.attemptsTo(Enter.theValue(password).into(EndPassword.INPUT_PASSWORD),
                Enter.theValue(confirmPassword).into(EndPassword.INPUT_CONFIRM_PASSWORD),
                Click.on(EndPassword.CHECK_TERMS),
                Click.on(EndPassword.CHECK_PRIVACY),
                Click.on(EndPassword.BUTTON_COMPLETE)
        );
    }
}
