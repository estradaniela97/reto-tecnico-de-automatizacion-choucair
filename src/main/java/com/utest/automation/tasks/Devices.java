package com.utest.automation.tasks;

import com.utest.automation.userinterface.NextDevices;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;

public class Devices implements Task {

    private String computer;
    private String version;
    private String language;
    private String mobile;
    private String model;
    private String system;

    public Devices(String computer, String version, String language, String mobile, String model, String system) {
        this.computer = computer;
        this.version = version;
        this.language = language;
        this.mobile = mobile;
        this.model = model;
        this.system = system;
    }

    public static Devices onThePage(String computer,String version,String language,String mobile,String model,String system) {
        return Tasks.instrumented(Devices.class,computer,version,language,mobile,model,system);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(NextDevices.CLICK_COMPUTER),
                Enter.theValue(computer).into(NextDevices.SELECT_COMPUTER),
                Hit.the(Keys.ENTER).keyIn(NextDevices.SELECT_COMPUTER),

                Click.on(NextDevices.CLICK_VERSION),
                Enter.theValue(version).into(NextDevices.SELECT_VERSION),
                Hit.the(Keys.ENTER).keyIn(NextDevices.SELECT_VERSION),

                Click.on(NextDevices.CLICK_LANGUAGE),
                Enter.theValue(language).into(NextDevices.SELECT_LANGUAGE),
                Hit.the(Keys.ENTER).keyIn(NextDevices.SELECT_LANGUAGE),

                Click.on(NextDevices.CLICK_MOBILE),
                Enter.theValue(mobile).into(NextDevices.SELECT_MOBILE),
                Hit.the(Keys.ENTER).keyIn(NextDevices.SELECT_MOBILE),

                Click.on(NextDevices.CLICK_MODEL),
                Enter.theValue(model).into(NextDevices.SELECT_MODEL),
                Hit.the(Keys.ENTER).keyIn(NextDevices.SELECT_MODEL),

                Click.on(NextDevices.CLICK_OPERATING_SYSTEM),
                Enter.theValue(system).into(NextDevices.SELECT_OPERATING_SYSTEM),
                Hit.the(Keys.ENTER).keyIn(NextDevices.SELECT_OPERATING_SYSTEM),

                Click.on(NextDevices.BUTTON_END)
                );
    }
}
