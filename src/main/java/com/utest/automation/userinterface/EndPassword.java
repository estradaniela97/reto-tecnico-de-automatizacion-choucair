package com.utest.automation.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EndPassword extends PageObject{
    public static final Target INPUT_PASSWORD = Target.the("Enter password").located(By.id("password"));
    public static final Target INPUT_CONFIRM_PASSWORD = Target.the("Confirm password").located(By.id("confirmPassword"));
    public static final Target CHECK_TERMS = Target.the("check terms of use").located(By.xpath("(//label[@class=\"input-check-box signup-consent\"]/span)[1]"));
    public static final Target CHECK_PRIVACY = Target.the("check privacy").located(By.xpath("//input[@id=\"privacySetting\"]"));
    public static final Target BUTTON_COMPLETE = Target.the("check privacy").located(By.xpath("//a[@id=\"laddaBtn\"]"));
}
