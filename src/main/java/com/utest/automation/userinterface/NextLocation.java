package com.utest.automation.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class NextLocation extends PageObject {
    public static final Target INPUT_CITY = Target.the("Input city").located(By.id("city"));
    public static final Target SEARCH_CITY = Target.the("Input city").located(By.id("city"));
    public static final Target INPUT_ZIP = Target.the("Input zip").located(By.id("zip"));
    public static final Target INPUT_COUNTRY = Target.the("Input country").located(By.xpath("//div[@class=\"ui-select-match\"]/span[@aria-label=\"Select a country\"]"));
    public static final Target INPUT_SEARCH_COUNTRY = Target.the("Input search country").located(By.xpath("//input[@placeholder=\"Select a country\"]"));
    public static final Target BUTTON_NEXT_DEVICES = Target.the("Next devices").located(By.xpath("//div[@class=\"pull-right next-step\"]"));
}
