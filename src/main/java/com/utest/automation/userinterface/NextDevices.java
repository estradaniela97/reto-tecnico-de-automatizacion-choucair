package com.utest.automation.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class NextDevices extends PageObject {
    public static final Target CLICK_COMPUTER = Target.the("Click span computer").located(By.xpath("(//div[@class=\"ui-select-match\"]/span)[1]"));
    public static final Target SELECT_COMPUTER = Target.the("Select span computer").located(By.xpath("(//input[@placeholder=\"Select OS\"])[1]"));
    public static final Target CLICK_VERSION = Target.the("Click span version").located(By.xpath("//span[@aria-label=\"Select a Version\"]"));
    public static final Target SELECT_VERSION = Target.the("Select span version").located(By.xpath("//input[@placeholder=\"Select a Version\"]"));
    public static final Target CLICK_LANGUAGE = Target.the("Click language").located(By.xpath("//span[@aria-label=\"Select OS language\"]"));
    public static final Target SELECT_LANGUAGE = Target.the("Select span language").located(By.xpath("//input[@placeholder=\"Select OS language\"]"));
    public static final Target CLICK_MOBILE = Target.the("Click span mobile").located(By.xpath("//span[@aria-label=\"Select Brand\"]"));
    public static final Target SELECT_MOBILE = Target.the("select span mobile").located(By.xpath("//input[@placeholder=\"Select Brand\"]"));
    public static final Target CLICK_MODEL = Target.the("Click span model").located(By.xpath("//span[@aria-label=\"Select a Model\"]"));
    public static final Target SELECT_MODEL = Target.the("select span model").located(By.xpath("//input[@placeholder=\"Select a Model\"]"));
    public static final Target CLICK_OPERATING_SYSTEM = Target.the("Click span operating system").located(By.xpath("//div[@name=\"handsetOSId\"]"));
    public static final Target SELECT_OPERATING_SYSTEM = Target.the("select span operating system").located(By.xpath("(//input[@placeholder=\"Select OS\"])[2]"));
    public static final Target BUTTON_END = Target.the("Button next").located(By.xpath("//a[@aria-label=\"Next - final step\"]"));
}
