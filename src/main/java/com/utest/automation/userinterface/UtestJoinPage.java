package com.utest.automation.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class UtestJoinPage extends PageObject {
    public static final Target JOIN_BUTTON = Target.the("Button that shows us the form to register").located(By.xpath("(//ul[@class=\"nav navbar-nav\"])[2]/li/a[@class=\"unauthenticated-nav-bar__sign-up\"]"));
    public static final Target INPUT_FIRST_NAME =  Target.the("First name").located(By.id("firstName"));
    public static final Target INPUT_LAST_NAME = Target.the("Last name").located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("Email").located(By.id("email"));
    public static final Target SELECT_BIRTH_MONT = Target.the("Birth mont").located(By.id("birthMonth"));
    public static final Target SELECT_BIRTH_DAY = Target.the("Birth day").located(By.name("birthDay"));
    public static final Target SELECT_BIRTH_YEAR = Target.the("Birth year").located(By.name("birthYear"));
    public static final Target BUTTON_NEXT = Target.the("Button next form").located(By.xpath("//div[@class=\"form-group col-xs-12 text-right\"]/a[@class=\"btn btn-blue\"]"));
}
