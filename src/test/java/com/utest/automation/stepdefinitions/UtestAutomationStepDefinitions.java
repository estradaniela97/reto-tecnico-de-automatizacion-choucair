package com.utest.automation.stepdefinitions;

import com.utest.automation.model.UtestAutomationData;
import com.utest.automation.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class UtestAutomationStepDefinitions {
    @Before
    public void setStage (){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^Daniela wants to register in utest and has to enter her data correctly in each form$")
    public void DanielaWantsToRegisterInUtestAndHasToEnterHerDataCorrectlyInEachForm(List<UtestAutomationData> utestAutomationData) throws Exception{
        OnStage.theActorCalled("Daniela").wasAbleTo(OpenUp.thePage(), Join.onThePage(utestAutomationData.get(0).getFirstName(),utestAutomationData.get(0).getLastName(),utestAutomationData.get(0).getEmail(),utestAutomationData.get(0).getMonth(),utestAutomationData.get(0).getDay(),utestAutomationData.get(0).getYear()));
    }

    @When("^She completes the first form and proceeds to the placement form She completes the second location form and continues with the device form$")
    public void SheCompletesTheFirstFormAndProceedsToThePlacementFormSheCompletesTheSecondLocationFormAndContinuesWithTheDeviceForm(List<UtestAutomationData> utestAutomationData) throws Exception{
        OnStage.theActorInTheSpotlight().wasAbleTo(Location.onThePage(utestAutomationData.get(0).getCity(),utestAutomationData.get(0).getZip(),utestAutomationData.get(0).getCountry()));
    }

    @When("^She completes the third form devices and advances to the last form$")
    public void SheCompletesTheThirdFormDevicesAndAdvancesToTheLastForm(List<UtestAutomationData> utestAutomationData) throws Exception{
        OnStage.theActorInTheSpotlight().wasAbleTo(Devices.onThePage(utestAutomationData.get(0).getComputer(),utestAutomationData.get(0).getVersion(),utestAutomationData.get(0).getLanguage(),utestAutomationData.get(0).getMobile(),utestAutomationData.get(0).getModel(),utestAutomationData.get(0).getSystem()));
    }
    @When("^She completes the last form$")
    public void SheCompletesTheLastForm(List<UtestAutomationData> utestAutomationData) throws Exception{
        OnStage.theActorInTheSpotlight().wasAbleTo(Password.onThePage(utestAutomationData.get(0).getPassword(),utestAutomationData.get(0).getConfirmPassword()));
    }

    @Then("^She registered correctly$")
    public void SheRegisteredCorrectly() {
    }
}
