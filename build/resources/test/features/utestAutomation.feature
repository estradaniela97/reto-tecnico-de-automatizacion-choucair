@stories
  Feature: Utest
    Create a new user in Utest
  @scenario1
  Scenario: User registration through four forms
    Given Daniela wants to register in utest and has to enter her data correctly in each form
      |firstName|lastName|        email            |  month  |   day   |   year    |
      | Daniela |Estrada | kmullerr_h76b@tigpe.com |number:10|number:29|number:1900|
    When She completes the first form and proceeds to the placement form She completes the second location form and continues with the device form
      |    city    |  zip  |  country  |
      |Chiquinquira|1234567|  Colombia  |
    When She completes the third form devices and advances to the last form
      |computer |  version  |language|   mobile   |model|  system   |
      | Windows |Windows 10 |English |BenQ-Siemens| C81 |Android 3.1|
    When She completes the last form
      |     password      |  confirmPassword  |
      |    Daniela123.    |    Daniela123.    |
    Then She registered correctly